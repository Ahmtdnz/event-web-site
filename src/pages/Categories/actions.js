import {
  GET_CATEGORIES,
  GET_CATEGORIES_SUCCESS,
  GET_CATEGORIES_FAIL,
  DELETE_CATEGORY,
  DELETE_CATEGORY_SUCCESS,
  DELETE_CATEGORY_FAIL,
} from './constants';

export function getCategories() {
  return {
    type: GET_CATEGORIES,
  };
}

export function getCategoriesSuccess(categories) {
  return {
    type: GET_CATEGORIES_SUCCESS,
    categories,
  };
}

export function getCategoriesFail(error) {
  return {
    type: GET_CATEGORIES_FAIL,
    error,
  };
}

export function deleteCategory(id) {
  return {
    type: DELETE_CATEGORY,
    id,
  };
}

export function deleteCategorySuccess(categories) {
  return {
    type: DELETE_CATEGORY_SUCCESS,
    categories,
  };
}

export function deleteCategoryFail(error) {
  return {
    type: DELETE_CATEGORY_FAIL,
    error,
  };
}
