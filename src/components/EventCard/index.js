import React, { Component } from 'react'

export default class EventCard extends Component {
  render() {
    return (
      <div className="col-6 col-lg-4" style={{display:'flex', flexDirection: 'column', marginBottom: 25}}>
        <img src="https://img.evbuc.com/https%3A%2F%2Fcdn.evbuc.com%2Fimages%2F36053517%2F173428071598%2F1%2Foriginal.jpg?h=230&amp;w=460&amp;rect=0%2C0%2C2000%2C1000&amp;s=8b39edc1ced97e1672812e4050db3a2d" alt="V-RAY ARCHITECTURE DAY ISTANBUL tickets" />
        <div style={{flexDirection: 'column', paddingLeft: 10}}>
          <div style={{fontSize: 12, color: '#45494E', marginTop: 3}}>10.10.2017</div>
          <h2 style={{color: '#282C35', fontSize: 20, marginTop: 3}}>Title</h2>
          <div style={{fontSize: 14, color: '#666A73', marginTop: 6}}>Location</div>
        </div>
      </div>
    )
  }
}
