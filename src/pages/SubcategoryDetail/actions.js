import {
  GET_SUBCATEGORY,
  GET_SUBCATEGORY_SUCCESS,
  GET_SUBCATEGORY_FAIL,
  UPDATE_SUBCATEGORY,
  UPDATE_SUBCATEGORY_SUCCESS,
  UPDATE_SUBCATEGORY_FAIL,
} from './constants';

export function getSubcategory(subcategoryId) {
  return {
    type: GET_SUBCATEGORY,
    subcategoryId,
  };
}

export function getSubcategorySuccess(event) {
  return {
    type: GET_SUBCATEGORY_SUCCESS,
    event,
  };
}

export function getSubcategoryFail(error) {
  return {
    type: GET_SUBCATEGORY_FAIL,
    error,
  };
}

export function updateSubcategory(data) {
  return {
    type: UPDATE_SUBCATEGORY,
    data,
  };
}

export function updateSubcategorySuccess(event) {
  return {
    type: UPDATE_SUBCATEGORY_SUCCESS,
    event,
  };
}

export function updateSubcategoryFail(error) {
  return {
    type: UPDATE_SUBCATEGORY_FAIL,
    error,
  };
}
