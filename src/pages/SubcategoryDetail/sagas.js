import { call, put } from 'redux-saga/effects';
import {
  getSubcategorySuccess,
  getSubcategoryFail,
  updateSubcategorySuccess,
  updateSubcategoryFail,
} from './actions';
import request from '../../utils/request';

const API = require('../../utils/api');

export default function* getCategory({ subcategoryId, type, data }) {
  if (type === 'GET_SUBCATEGORY') {
    const subcategoryRequestURL = `${API.api_url}subcategories/${subcategoryId}?access_token=${localStorage.getItem('token')}`;
    try {
      const subcategoryResult = yield call(request, subcategoryRequestURL, 'GET');
      yield put(getSubcategorySuccess(subcategoryResult));
    } catch (error) {
      yield put(getSubcategoryFail());
    }
  } else if (type === 'UPDATE_SUBCATEGORY') {
    const updateSubcategoryRequestURL = `${API.api_url}subcategories?access_token=${localStorage.getItem('token')}`;
    try {
      const updateSubcategoryResult = yield call(request, updateSubcategoryRequestURL, 'PUT', data);
      yield put(updateSubcategorySuccess(updateSubcategoryResult));
    } catch (error) {
      yield put(updateSubcategoryFail());
    }
  }
}
