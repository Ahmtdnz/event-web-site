import {
  ADD_SUBCATEGORY,
  ADD_SUBCATEGORY_SUCCESS,
  ADD_SUBCATEGORY_FAIL,
} from './constants';

export function addSubcategory(data) {
  return {
    type: ADD_SUBCATEGORY,
    data,
  };
}

export function addSubcategorySuccess(events) {
  return {
    type: ADD_SUBCATEGORY_SUCCESS,
    events,
  };
}

export function addSubcategoryFail(error) {
  return {
    type: ADD_SUBCATEGORY_FAIL,
    error,
  };
}
