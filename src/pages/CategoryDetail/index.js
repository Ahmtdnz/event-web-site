import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import Select from 'react-select';
import { withRouter } from 'react-router-dom';
import { getCategory, updateCategory } from './actions';

class CategoryDetail extends Component {
  componentWillMount() {
    let { match: {params} } = this.props;
    this.props.dispatch(getCategory(params.id));
  }

  componentWillReceiveProps = (nextProps) => {
    let { form, getCategory } = this.props;
    if(Object.keys(nextProps.getCategory.category).length > 0) {
      form.values = nextProps.getCategory.category;
    }
    if(nextProps.getCategory.updateCategoryProcess) {
      this.props.history.push('/categories');
    }
  }

  submit(e) {
    e.preventDefault();
    let { form, dispatch } = this.props;
    dispatch(updateCategory(form.values));
  }

  render() {
    let { form } = this.props;
    return (
      <div className="container" style={{marginTop: 75}}>
        <form className="col-md-6 col-md-offset-3">
          <div className="form-group">
            <label>Category Name</label>
            <Field
              name="name"
              component="input"
              className="form-control"
              type="text"
              placeholder="Name"
            />
          </div>
          <button onClick={(e) => this.submit(e)} className="btn btn-primary">Submit</button>
        </form>
      </div>
    );
  }
}

const mapToProps = ({ form, getCategory }) => (
  {
    form: form['add-category'],
    getCategory,
  }
);

const wr = withRouter(connect(mapToProps)(CategoryDetail));

export default reduxForm({
  form: 'add-category',
  initialValues: {
    name: '',
  },
})(wr);
