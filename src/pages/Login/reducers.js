import {
  LOGIN,
  LOGIN_SUCCESS,
  LOGIN_FAIL,
} from './constants';


const initial = {
  login: {
    loginProcess: '',
  },
};

export default function app(state = initial.login, action) {
  switch (action.type) {
    case LOGIN:
      return { ...state };
    case LOGIN_SUCCESS:
      return { ...state, loginProcess: true };
    case LOGIN_FAIL:
      return { ...state, loginProcess: false };
    default:
      return state;
  }
}
