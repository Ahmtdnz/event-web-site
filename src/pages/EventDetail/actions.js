import {
  GET_EVENT,
  GET_EVENT_SUCCESS,
  GET_EVENT_FAIL,
  UPDATE_EVENT,
  UPDATE_EVENT_SUCCESS,
  UPDATE_EVENT_FAIL,
} from './constants';

export function getEvent(eventId) {
  return {
    type: GET_EVENT,
    eventId,
  };
}

export function getEventSuccess(event) {
  return {
    type: GET_EVENT_SUCCESS,
    event,
  };
}

export function getEventFail(error) {
  return {
    type: GET_EVENT_FAIL,
    error,
  };
}

export function updateEvent(data) {
  return {
    type: UPDATE_EVENT,
    data,
  };
}

export function updateEventSuccess(event) {
  return {
    type: UPDATE_EVENT_SUCCESS,
    event,
  };
}

export function updateEventFail(error) {
  return {
    type: UPDATE_EVENT_FAIL,
    error,
  };
}
