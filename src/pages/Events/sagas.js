import { call, put, fork } from 'redux-saga/effects';
import {
  getEvents,
  getEventsSuccess,
  getEventsFail,
  deleteEventSuccess,
  deleteEventFail,
} from './actions';
import request from '../../utils/request';

const API = require('../../utils/api');

function* deleteEvent(eventId) {
  const deleteRequestURL = `${API.api_url}events/${eventId}?access_token=${localStorage.getItem('token')}`;
  try {
    const deleteEventResult = yield call(request, deleteRequestURL, 'DELETE');
    yield put(deleteEventSuccess(deleteEventResult));
    yield put(getEvents());
  } catch (error) {
    yield put(deleteEventFail());
  }
}

export default function* getEvent({ type, eventId }) {
  if (type === 'DELETE_EVENT') {
    yield fork(deleteEvent, eventId);
  } else if (type === 'GET_EVENTS') {
    const eventsRequestURL = `${API.api_url}events?access_token=${localStorage.getItem('token')}`;
    try {
      const eventsResult = yield call(request, eventsRequestURL, 'GET');
      yield put(getEventsSuccess(eventsResult));
    } catch (error) {
      yield put(getEventsFail());
    }
  }
}
