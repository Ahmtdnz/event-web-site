import {
  GET_CATEGORY,
  GET_CATEGORY_SUCCESS,
  GET_CATEGORY_FAIL,
  UPDATE_CATEGORY,
  UPDATE_CATEGORY_SUCCESS,
  UPDATE_CATEGORY_FAIL,
} from './constants';


const initial = {
  category: {
    category: {},
    updateCategoryProcess: '',
  },
};

export default function app(state = initial.category, action) {
  switch (action.type) {
    case GET_CATEGORY:
      return { ...state };
    case GET_CATEGORY_SUCCESS:
      return { ...state, category: action.event.data };
    case GET_CATEGORY_FAIL:
      return { ...state, error: 'error' };
    case UPDATE_CATEGORY:
      return { ...state };
    case UPDATE_CATEGORY_SUCCESS:
      return { ...state, updateCategoryProcess: true };
    case UPDATE_CATEGORY_FAIL:
      return { ...state, updateCategoryProcess: false };
    default:
      return state;
  }
}
