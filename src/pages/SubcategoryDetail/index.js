import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import Select from 'react-select';
import { withRouter } from 'react-router-dom';
import { getSubcategory, updateSubcategory } from './actions';
import { getCategories } from '../Categories/actions';

class SubcategoryDetail extends Component {
  componentWillMount() {
    let { match: {params} } = this.props;
    this.props.dispatch(getCategories());
    this.props.dispatch(getSubcategory(params.id));
  }

  componentWillReceiveProps = (nextProps) => {
    let { form, getCategory } = this.props;
    if(Object.keys(nextProps.subcategory.subcategory).length > 0) {
      form.values = nextProps.subcategory.subcategory;
    }
    if(nextProps.subcategory.updateSubcategoryProcess) {
      this.props.history.push('/subcategories');
    }
  }

  submit(e) {
    e.preventDefault();
    let { form, dispatch } = this.props;
    dispatch(updateSubcategory(form.values));
  }

  render() {
    let { form, categories } = this.props;
    return (
      <div className="container" style={{marginTop: 75}}>
        <form className="col-md-6 col-md-offset-3">
          <div className="form-group">
            <label>Category Name</label>
            <Field
              name="categoryId"
              component={ props =>
                <Select
                  name="form-field-name"
                  value={ props.input.value }
                  labelKey="name"
                  valueKey="id"
                  options={categories.categories}
                  onChange={(value) => props.input.onChange(value)}
                  onBlur={() => props.input.onBlur(props.input.value)}
                />
              }
            />
          </div>
          <div className="form-group">
            <label>Subcategory Name</label>
            <Field
              name="name"
              component="input"
              className="form-control"
              type="text"
              placeholder="Name"
            />
          </div>
          <button onClick={(e) => this.submit(e)} className="btn btn-primary">Submit</button>
        </form>
      </div>
    );
  }
}

const mapToProps = ({ form, subcategory, categories }) => (
  {
    form: form['add-subcategory'],
    subcategory,
    categories,
  }
);

const wr = withRouter(connect(mapToProps)(SubcategoryDetail));

export default reduxForm({
  form: 'add-subcategory',
  initialValues: {
    name: '',
    categoryId: '',
  },
})(wr);
