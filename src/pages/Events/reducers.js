import {
  GET_EVENTS,
  GET_EVENTS_SUCCESS,
  GET_EVENTS_FAIL,
  DELETE_EVENT,
  DELETE_EVENT_SUCCESS,
  DELETE_EVENT_FAIL,
} from './constants';

const initial = {
  app: {
    events: [],
    deleteProcess: '',
  },
};

export default function app(state = initial.app, action) {
  switch (action.type) {
    case GET_EVENTS:
      return { ...state };
    case GET_EVENTS_SUCCESS:
      return { ...state, events: action.events.data };
    case GET_EVENTS_FAIL:
      return { ...state, error: 'error' };
    case DELETE_EVENT:
      return { ...state };
    case DELETE_EVENT_SUCCESS:
      return { ...state, deleteProcess: true };
    case DELETE_EVENT_FAIL:
      return { ...state, deleteProcess: false };
    default:
      return state;
  }
}
