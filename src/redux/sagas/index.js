import { fork, takeEvery } from 'redux-saga/effects';
import getEvents from '../../pages/Events/sagas';
import getCategories from '../../pages/Categories/sagas';
import getSubCategory from '../../pages/SubCategories/sagas';
import getEvent from '../../pages/EventDetail/sagas';
import addEvent from '../../pages/AddEvent/sagas';
import addCategory from '../../pages/AddCategory/sagas';
import getCategory from '../../pages/CategoryDetail/sagas';
import addSubcategory from '../../pages/AddSubcategory/sagas';
import getSubcategoryDetail from '../../pages/SubcategoryDetail/sagas';
import login from '../../pages/Login/sagas';

export default function* rootSaga() {
  yield fork(takeEvery, 'GET_EVENTS', getEvents);
  yield fork(takeEvery, 'GET_CATEGORIES', getCategories);
  yield fork(takeEvery, 'GET_SUBCATEGORIES', getSubCategory);
  yield fork(takeEvery, 'GET_EVENT', getEvent);
  yield fork(takeEvery, 'DELETE_EVENT', getEvents);
  yield fork(takeEvery, 'ADD_EVENT', addEvent);
  yield fork(takeEvery, 'UPDATE_EVENT', getEvent);
  yield fork(takeEvery, 'ADD_CATEGORY', addCategory);
  yield fork(takeEvery, 'DELETE_CATEGORY', getCategories);
  yield fork(takeEvery, 'GET_CATEGORY', getCategory);
  yield fork(takeEvery, 'UPDATE_CATEGORY', getCategory);
  yield fork(takeEvery, 'DELETE_SUBCATEGORY', getSubCategory);
  yield fork(takeEvery, 'ADD_SUBCATEGORY', addSubcategory);
  yield fork(takeEvery, 'GET_SUBCATEGORY', getSubcategoryDetail);
  yield fork(takeEvery, 'UPDATE_SUBCATEGORY', getSubcategoryDetail);
  yield fork(takeEvery, 'LOGIN', login);
}
