import React, { Component } from 'react';
import * as classNames from 'classnames';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';
import { BootstrapTable, TableHeaderColumn, DeleteButton, InsertButton } from 'react-bootstrap-table';
import { getSubcategories, deleteSubcategory } from './actions';

class Categories extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedItem: '',
    };
    this.handleRowSelect = this.handleRowSelect.bind(this);
    this.createCustomDeleteButton = this.createCustomDeleteButton.bind(this);
    this.createCustomInsertButton = this.createCustomInsertButton.bind(this);
  }
  componentWillMount() {
    this.props.dispatch(getSubcategories());
  }

  handleRowSelect(row) {
    this.setState({ selectedItem: row });
  }

  createCustomDeleteButton(onClick) {
    const wrapperClass = classNames({
      'btn': true,
      'btn-warning': true,
      'disabled': this.state.selectedItem === '',
    });
    return (
      <DeleteButton
        btnText='Delete'
        btnContextual='btn-warning'
        className={wrapperClass}
        btnGlyphicon='glyphicon-remove'
        onClick={() => this.delete()}
      />
    );
  }

  delete() {
    let { dispatch } = this.props;
    dispatch(deleteSubcategory(this.state.selectedItem.id));
    this.setState({ selectedItem: '' });
  }

  createCustomInsertButton = (onClick) => {
    const wrapperClass = classNames({
      'btn': true,
      'btn-info': true,
      'disabled': this.state.selectedItem === '',
    });
    return (
      <InsertButton
        btnText='Category Detail'
        btnContextual='btn-info'
        className={wrapperClass}
        btnGlyphicon='glyphicon-edit'
        onClick={ () => this.props.history.push(`/subcategory/${this.state.selectedItem.id}`) }
      />
    );
  }

  render() {
    const options = {
      deleteBtn: this.createCustomDeleteButton,
      insertBtn: this.createCustomInsertButton,
    };
    const selectRow = {
      mode: 'radio',
      onSelect: this.handleRowSelect,
    };
    let { subcategories } = this.props;
    return (
      <div className="container" style={{marginTop: 75}}>
        <Link
          to="/subcategory/add"
          className="btn btn-success"
          style={{marginBottom: 20}}
        >
          Add Category
        </Link>
        <BootstrapTable
          data={subcategories.subcategories}
          striped
          hover
          options={options}
          selectRow={selectRow}
          deleteRow
          insertRow
        >
          <TableHeaderColumn isKey dataField="id">Id</TableHeaderColumn>
          <TableHeaderColumn dataField="name">Name</TableHeaderColumn>
          <TableHeaderColumn dataField="categoryId">Category Id</TableHeaderColumn>
        </BootstrapTable>
      </div>
    );
  }
}

const mapToProps = ({ subcategories }) => (
  {
    subcategories,
  }
);

export default withRouter(connect(mapToProps)(Categories));
