import { call, put } from 'redux-saga/effects';
import {
  addEventSuccess,
  addEventFail,
} from './actions';
import request from '../../utils/request';

const API = require('../../utils/api');

export default function* addEvent({ data }) {
  const addEventRequestURL = `${API.api_url}events?access_token=${localStorage.getItem('token')}`;
  try {
    const eventsResult = yield call(request, addEventRequestURL, 'POST', data);
    yield put(addEventSuccess(eventsResult));
  } catch (error) {
    yield put(addEventFail());
  }
}
