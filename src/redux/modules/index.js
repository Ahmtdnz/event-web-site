import {
  combineReducers,
} from 'redux';
import { reducer as reduxFormReducer } from 'redux-form';
import events from '../../pages/Events/reducers';
import event from '../../pages/EventDetail/reducers';
import categories from '../../pages/Categories/reducers';
import getCategory from '../../pages/CategoryDetail/reducers';
import subcategories from '../../pages/SubCategories/reducers';
import addEvent from '../../pages/AddEvent/reducers';
import addCategory from '../../pages/AddCategory/reducers';
import addSubcategory from '../../pages/AddSubcategory/reducers';
import subcategory from '../../pages/SubcategoryDetail/reducers';
import login from '../../pages/Login/reducers';

export default combineReducers({
  events,
  event,
  categories,
  subcategories,
  addSubcategory,
  addEvent,
  addCategory,
  getCategory,
  subcategory,
  login,
  form: reduxFormReducer,
});
