import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import Select from 'react-select';
import { withRouter } from 'react-router-dom';
import { addSubcategory } from './actions';
import { getCategories } from '../Categories/actions';

class AddSubcategory extends Component {
  componentWillMount() {
    this.props.dispatch(getCategories());
  }
  componentWillReceiveProps = (nextProps) => {
    if (nextProps.addSubcategory.addSubcategoryProcess) {
      this.props.history.push('/subcategories');
    }
  }
  
  componentWillUnmount = () => {
    this.props.addSubcategory.addSubcategoryProcess = '';
  }

  submit(e) {
    e.preventDefault();
    let { form, dispatch } = this.props;
    dispatch(addSubcategory(form.values));
  }

  render() {
    let { form, categories } = this.props;
    return (
      <div className="container" style={{marginTop: 75}}>
        <form className="col-md-6 col-md-offset-3">
          <div className="form-group">
            <label>Category</label>
            <Field
              name="categoryId"
              component={ props =>
                <Select
                  name="form-field-name"
                  value={ props.input.value }
                  labelKey="name"
                  valueKey="id"
                  options={categories}
                  onChange={(value) => props.input.onChange(value.id)}
                  onBlur={() => props.input.onBlur(props.input.value)}
                />
              }
            />
          </div>
          <div className="form-group">
            <label>Subcategory Name</label>
            <Field
              name="name"
              component="input"
              className="form-control"
              type="text"
              placeholder="Name"
            />
          </div>
          <button onClick={(e) => this.submit(e)} className="btn btn-primary">Submit</button>
        </form>
      </div>
    );
  }
}

const mapToProps = ({ form, addSubcategory, categories }) => (
  {
    form: form['add-subcategory'],
    addSubcategory,
    categories: categories.categories,
  }
);

const wr = withRouter(connect(mapToProps)(AddSubcategory));

export default reduxForm({
  form: 'add-subcategory',
  initialValues: {
    name: '',
    categoryId: ''
  },
})(wr);
