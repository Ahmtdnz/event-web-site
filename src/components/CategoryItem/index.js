import React from 'react';
import { NavLink } from 'react-router-dom';

const CategoryItem = () => {
  return (
    <NavLink to="/" activeClassName="active" className="list-group-item">Link </NavLink>
  );
}

export default CategoryItem;
