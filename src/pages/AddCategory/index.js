import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import { withRouter } from 'react-router-dom';
import { addCategory } from './actions';

class AddCategory extends Component {
  componentWillReceiveProps(nextProps) {
    if (nextProps.addCategory.addCategoryProcess) {
      this.props.history.push('/categories');
    }
  }

  componentWillUnmount() {
    this.props.addCategory.addCategoryProcess = '';
  }

  submit(e) {
    e.preventDefault();
    let { form, dispatch } = this.props;
    dispatch(addCategory(form.values));
  }

  render() {
    let { form } = this.props;
    return (
      <div className="container" style={{marginTop: 75}}>
        <form className="col-md-6 col-md-offset-3">
          <div className="form-group">
            <label>Category Name</label>
            <Field
              name="name"
              component="input"
              className="form-control"
              type="text"
              placeholder="Name"
            />
          </div>
          <button onClick={e => this.submit(e)} className="btn btn-primary">Submit</button>
        </form>
      </div>
    );
  }
}

const mapToProps = ({ form, addCategory }) => (
  {
    form: form['add-category'],
    addCategory,
  }
);

const wr = withRouter(connect(mapToProps)(AddCategory));

export default reduxForm({
  form: 'add-category',
  initialValues: {
    name: '',
  },
})(wr);
