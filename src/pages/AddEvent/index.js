import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import Select from 'react-select';
import { withRouter } from 'react-router-dom';
import { getCategories } from '../Categories/actions';
import { getSubcategories } from '../SubCategories/actions';
import { addEvent } from './actions';

class AddEvent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      file: '',
      imagePreviewUrl: '',
    };
  }

  componentWillMount() {
    this.props.dispatch(getCategories());
    this.props.dispatch(getSubcategories());
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.addEvent.addEventProcess) {
      this.props.history.push('/');
    }
  }

  componentWillUnmount() {
    this.props.addEvent.addEventProcess = '';
  }

  handleImageChange(e) {
    e.preventDefault();

    const reader = new FileReader();
    const file = e.target.files[0];

    reader.onloadend = () => {
      this.setState({
        file,
        imagePreviewUrl: reader.result,
      });
    };
    reader.readAsDataURL(file);
  }

  submit(e) {
    e.preventDefault();
    let { form, dispatch } = this.props;
    form.values['image'] = this.state.imagePreviewUrl;
    dispatch(addEvent(form.values));
  }

  render() {
    let { form, categories, subcategories } = this.props;
    const { imagePreviewUrl } = this.state;
    let $imagePreview = null;
    if (imagePreviewUrl) {
      $imagePreview = (<img src={imagePreviewUrl} alt="" />);
    } else {
      $imagePreview = (<div className="previewText">Please select an Image for Preview</div>);
    }
    return (
      <div className="container" style={{marginTop: 75}}>
        <form className="col-md-6 col-md-offset-3">
          <div className="form-group">
            <label>Category</label>
            <Field
              name="category"
              component={props =>
                <Select
                  name="form-field-name"
                  value={ props.input.value }
                  labelKey="name"
                  valueKey="id"
                  options={categories}
                  onChange={value => props.input.onChange(value.id)}
                  onBlur={() => props.input.onBlur(props.input.value)}
                />
              }
            />
          </div>
          <div className="form-group">
            <label>Subcategory</label>
            <Field
              name="subcategoryId"
              component={ props =>
                <Select
                  name="form-field-name"
                  value={ props.input.value }
                  labelKey="name"
                  valueKey="id"
                  options={subcategories}
                  onChange={(value) => props.input.onChange(value.id)}
                  onBlur={() => props.input.onBlur(props.input.value)}
                />
              }
            />
          </div>
          <div className="form-group">
            <label>Title</label>
            <Field
              name="title"
              component="input"
              className="form-control"
              type="text"
              placeholder="Title"
            />
          </div>
          <div className="form-group">
            <label>Description</label>
            <Field
              name="description"
              component="textarea"
              className="form-control"
              type="text"
              placeholder="Description"
            />
          </div>
          <div className="form-group">
            <label>Image</label>
            <input 
              className="fileInput"
              type="file"
              accept="image/*"
              onChange={e => this.handleImageChange(e)}
            />
          </div>
          <div className="imgPreview">
            {$imagePreview}
          </div>
          <button onClick={e => this.submit(e)} className="btn btn-primary">Submit</button>
        </form>
      </div>
    );
  }
}

const mapToProps = ({ form, categories, subcategories, addEvent }) => (
  {
    form: form['add-event'],
    categories: categories.categories,
    subcategories: subcategories.subcategories,
    addEvent,
  }
);

const wr = withRouter(connect(mapToProps)(AddEvent));

export default reduxForm({
  form: 'add-event',
  initialValues: {
    title: '',
    description: '',
    category: '',
    subcategoryId: '',
  },
})(wr);
