import { call, put } from 'redux-saga/effects';
import {
  getSubcategories,
  getSubcategoriesSuccess,
  getSubcategoriesFail,
  deleteSubcategorySuccess,
  deleteSubcategoryFail,
} from './actions';
import request from '../../utils/request';

const API = require('../../utils/api');

export default function* getSubCategory({ type, id }) {
  if (type === 'DELETE_SUBCATEGORY') {
    const deleteSubcategoryRequestURL = `${API.api_url}subcategories/${id}?access_token=${localStorage.getItem('token')}`;
    try {
      const deleteSubcategoryResult = yield call(request, deleteSubcategoryRequestURL, 'DELETE');
      yield put(deleteSubcategorySuccess(deleteSubcategoryResult));
      yield put(getSubcategories());
    } catch (error) {
      yield put(deleteSubcategoryFail());
    }
  } else if (type === 'GET_SUBCATEGORIES') {
    const getSubcategoriesRequestURL = `${API.api_url}subcategories?access_token=${localStorage.getItem('token')}`;
    try {
      const getSubcategoriesResult = yield call(request, getSubcategoriesRequestURL, 'GET');
      yield put(getSubcategoriesSuccess(getSubcategoriesResult));
    } catch (error) {
      yield put(getSubcategoriesFail());
    }
  }
}
