import * as axios from 'axios';

axios.interceptors.response.use((response) => {
  return response;
}, (error) => {
  if (error.response.status === 401 && error.response.data.error === 'invalid_token') {
    window.location.href = '/logout';
  } 
  return Promise.reject(error);
});

export default function request(url, method, data) {
  return axios(url, {
    method,
    credentials: 'same-origin',
    headers: {
      'Content-Type': 'application/json',
    },
    data,
  });
}
