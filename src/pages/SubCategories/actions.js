import {
  GET_SUBCATEGORIES,
  GET_SUBCATEGORIES_SUCCESS,
  GET_SUBCATEGORIES_FAIL,
  DELETE_SUBCATEGORY,
  DELETE_SUBCATEGORY_SUCCESS,
  DELETE_SUBCATEGORY_FAIL,
} from './constants';

export function getSubcategories() {
  return {
    type: GET_SUBCATEGORIES,
  };
}

export function getSubcategoriesSuccess(categories) {
  return {
    type: GET_SUBCATEGORIES_SUCCESS,
    categories,
  };
}

export function getSubcategoriesFail(error) {
  return {
    type: GET_SUBCATEGORIES_FAIL,
    error,
  };
}

export function deleteSubcategory(id) {
  return {
    type: DELETE_SUBCATEGORY,
    id,
  };
}

export function deleteSubcategorySuccess(categories) {
  return {
    type: DELETE_SUBCATEGORY_SUCCESS,
    categories,
  };
}

export function deleteSubcategoryFail(error) {
  return {
    type: DELETE_SUBCATEGORY_FAIL,
    error,
  };
}
