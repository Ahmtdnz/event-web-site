import {
  ADD_EVENT,
  ADD_EVENT_SUCCESS,
  ADD_EVENT_FAIL,
} from './constants';

export function addEvent(data) {
  return {
    type: ADD_EVENT,
    data,
  };
}

export function addEventSuccess(events) {
  return {
    type: ADD_EVENT_SUCCESS,
    events,
  };
}

export function addEventFail(error) {
  return {
    type: ADD_EVENT_FAIL,
    error,
  };
}
