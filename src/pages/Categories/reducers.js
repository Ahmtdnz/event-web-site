import {
  GET_CATEGORIES,
  GET_CATEGORIES_SUCCESS,
  GET_CATEGORIES_FAIL,
  DELETE_CATEGORY,
  DELETE_CATEGORY_SUCCESS,
  DELETE_CATEGORY_FAIL,
} from './constants';


const initial = {
  categories: {
    categories: [],
    deleteCategoryProcess: '',
  },
};

export default function app(state = initial.categories, action) {
  switch (action.type) {
    case GET_CATEGORIES:
      return { ...state };
    case GET_CATEGORIES_SUCCESS:
      return { ...state, categories: action.categories.data };
    case GET_CATEGORIES_FAIL:
      return { ...state };
    case DELETE_CATEGORY:
      return { ...state };
    case DELETE_CATEGORY_SUCCESS:
      return { ...state, deleteCategoryProcess: true };
    case DELETE_CATEGORY_FAIL:
      return { ...state, deleteCategoryProcess: false };
    default:
      return state;
  }
}
