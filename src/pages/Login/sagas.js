import { call, put } from 'redux-saga/effects';
import {
  loginSuccess,
  loginFail,
} from './actions';
import request from '../../utils/request';

const API = require('../../utils/api');

export default function* addSubcategory({ data }) {
  const loginRequestURL = `${API.api_url}Users/login`;
  try {
    const loginResult = yield call(request, loginRequestURL, 'POST', data);
    if (loginResult.data.id.length > 0) {
      localStorage.token = loginResult.data.id;
      yield put(loginSuccess(loginResult));
    } else {
      yield put(loginFail());
    }
  } catch (error) {
    yield put(loginFail());
  }
}
