import {
  LOGIN,
  LOGIN_SUCCESS,
  LOGIN_FAIL,
} from './constants';

export function login(data) {
  return {
    type: LOGIN,
    data,
  };
}

export function loginSuccess(login) {
  return {
    type: LOGIN_SUCCESS,
    login,
  };
}

export function loginFail(error) {
  return {
    type: LOGIN_FAIL,
    error,
  };
}
