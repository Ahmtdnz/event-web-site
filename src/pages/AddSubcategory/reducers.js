import {
  ADD_SUBCATEGORY,
  ADD_SUBCATEGORY_SUCCESS,
  ADD_SUBCATEGORY_FAIL,
} from './constants';


const initial = {
  addSubcategory: {
    addSubcategoryProcess: '',
  },
};

export default function app(state = initial.addSubcategory, action) {
  switch (action.type) {
    case ADD_SUBCATEGORY:
      return { ...state };
    case ADD_SUBCATEGORY_SUCCESS:
      return { ...state, addSubcategoryProcess: true };
    case ADD_SUBCATEGORY_FAIL:
      return { ...state, addSubcategoryProcess: false };
    default:
      return state;
  }
}
