import {
  ADD_CATEGORY,
  ADD_CATEGORY_SUCCESS,
  ADD_CATEGORY_FAIL,
} from './constants';

export function addCategory(data) {
  return {
    type: ADD_CATEGORY,
    data,
  };
}

export function addCategorySuccess(events) {
  return {
    type: ADD_CATEGORY_SUCCESS,
    events,
  };
}

export function addCategoryFail(error) {
  return {
    type: ADD_CATEGORY_FAIL,
    error,
  };
}
