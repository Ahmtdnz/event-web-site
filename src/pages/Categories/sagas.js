import { call, put } from 'redux-saga/effects';
import {
  getCategories,
  getCategoriesSuccess,
  getCategoriesFail,
  deleteCategorySuccess,
  deleteCategoryFail,
} from './actions';
import request from '../../utils/request';

const API = require('../../utils/api');

export default function* addCategory({ type, id }) {
  if (type === 'DELETE_CATEGORY') {
    const deleteCategoryRequestURL = `${API.api_url}categories/${id}?access_token=${localStorage.getItem('token')}`;
    try {
      const deleteCategoryResult = yield call(request, deleteCategoryRequestURL, 'DELETE');
      yield put(deleteCategorySuccess(deleteCategoryResult));
      yield put(getCategories());
    } catch (error) {
      yield put(deleteCategoryFail());
    }
  } else if (type === 'GET_CATEGORIES') {
    const getCategoriesRequestURL = `${API.api_url}categories?access_token=${localStorage.getItem('token')}`;
    try {
      const getCategoriesResult = yield call(request, getCategoriesRequestURL, 'GET');
      yield put(getCategoriesSuccess(getCategoriesResult));
    } catch (error) {
      yield put(getCategoriesFail());
    }
  }
}
