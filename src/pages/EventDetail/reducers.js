import {
  GET_EVENT,
  GET_EVENT_SUCCESS,
  GET_EVENT_FAIL,
  UPDATE_EVENT,
  UPDATE_EVENT_SUCCESS,
  UPDATE_EVENT_FAIL,
} from './constants';


const initial = {
  event: {
    event: {},
    updateEventProcess: '',
  },
};

export default function app(state = initial.event, action) {
  switch (action.type) {
    case GET_EVENT:
      return { ...state };
    case GET_EVENT_SUCCESS:
      return { ...state, event: action.event.data };
    case GET_EVENT_FAIL:
      return { ...state, error: 'error' };
    case UPDATE_EVENT:
      return { ...state };
    case UPDATE_EVENT_SUCCESS:
      return { ...state, updateEventProcess: true };
    case UPDATE_EVENT_FAIL:
      return { ...state, updateEventProcess: false };
    default:
      return state;
  }
}
