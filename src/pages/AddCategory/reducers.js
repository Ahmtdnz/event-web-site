import {
  ADD_CATEGORY,
  ADD_CATEGORY_SUCCESS,
  ADD_CATEGORY_FAIL,
} from './constants';


const initial = {
  addCategory: {
    addCategoryProcess: '',
  },
};

export default function app(state = initial.addCategory, action) {
  switch (action.type) {
    case ADD_CATEGORY:
      return { ...state };
    case ADD_CATEGORY_SUCCESS:
      return { ...state, addCategoryProcess: true };
    case ADD_CATEGORY_FAIL:
      return { ...state, addCategoryProcess: false };
    default:
      return state;
  }
}
