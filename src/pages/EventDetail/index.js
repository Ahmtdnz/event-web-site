import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import Select from 'react-select';
import { withRouter, Link } from 'react-router-dom';
import { getCategories } from '../Categories/actions';
import { getSubcategories } from '../SubCategories/actions';
import { getEvent, updateEvent } from './actions';

class EventDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      file: '',
      imagePreviewUrl: '',
    };
  }
  componentWillMount() {
    let { match: {params} } = this.props;
    this.props.dispatch(getCategories());
    this.props.dispatch(getSubcategories());
    this.props.dispatch(getEvent(params.id));
  }

  update(e) {
    e.preventDefault();
    let { form, categories, subcategories, event, dispatch } = this.props;
    form.values['image'] = this.state.imagePreviewUrl;
    dispatch(updateEvent(form.values));
  }

  _handleImageChange(e) {
    e.preventDefault();

    const reader = new FileReader();
    const file = e.target.files[0];

    reader.onloadend = () => {
      this.setState({
        file,
        imagePreviewUrl: reader.result,
      });
    };
    reader.readAsDataURL(file);
  }
  
  componentWillReceiveProps = (nextProps) => {
    let { form, categories, subcategories, event } = this.props;
    if(Object.keys(nextProps.event.event).length > 0) {
      this.setState({ imagePreviewUrl: nextProps.event.event.image })
      form.values = nextProps.event.event;
    }
    if(nextProps.event.updateEventProcess) {
      this.props.history.push('/');
    }
  }

  componentWillUnmount = () => {
    this.props.event.updateEventProcess = '';
  }
  

  render() {
    let { form, categories, subcategories, event } = this.props;
    let {imagePreviewUrl} = this.state;
    let $imagePreview = null;
    if (imagePreviewUrl) {
      $imagePreview = (<img src={imagePreviewUrl} />);
    } else {
      $imagePreview = (<div className="previewText">Please select an Image for Preview</div>);
    }
    return (
      <div className="container" style={{marginTop: 75}}>
        <form className="col-md-6 col-md-offset-3">
          <div className="form-group">
            <label>Subcategory</label>
            <Field
              name="subcategoryId"
              component={ props =>
                <Select
                  name="form-field-name"
                  value={ props.input.value }
                  labelKey="name"
                  valueKey="id"
                  options={subcategories}
                  onChange={(value) => props.input.onChange(value)}
                  onBlur={() => props.input.onBlur(props.input.value)}
                />
              }
            />
          </div>
          <div className="form-group">
            <label>Title</label>
            <Field
              name="title"
              component="input"
              className="form-control"
              type="text"
              placeholder="Title"
            />
          </div>
          <div className="form-group">
            <label>Description</label>
            <Field
              name="description"
              component="textarea"
              className="form-control"
              type="text"
              placeholder="Description"
            />
          </div>
          <div className="form-group">
            <label>Image</label>
            <input 
              className="fileInput"
              type="file"
              accept="image/*"
              onChange={(e)=>this._handleImageChange(e)}
            />
          </div>
          <div className="imgPreview">
            {$imagePreview}
          </div>
          <button onClick={(e) => this.update(e)} type="submit" className="btn btn-primary">Update</button>
        </form>
      </div>
    );
  }
}

const mapToProps = ({ form, categories, subcategories, event }) => (
  {
    form: form['add-event'],
    categories: categories.categories,
    subcategories: subcategories.subcategories,
    event,
  }
);

const wr = withRouter(connect(mapToProps)(EventDetail));

export default reduxForm({
  form: 'add-event',
  initialValues: {
    title: '',
    description: '',
    subcategoryId: '',
  },
})(wr);
