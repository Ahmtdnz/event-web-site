import {
  GET_EVENTS,
  GET_EVENTS_SUCCESS,
  GET_EVENTS_FAIL,
  DELETE_EVENT,
  DELETE_EVENT_SUCCESS,
  DELETE_EVENT_FAIL,
} from './constants';

export function getEvents() {
  return {
    type: GET_EVENTS,
  };
}

export function getEventsSuccess(events) {
  return {
    type: GET_EVENTS_SUCCESS,
    events,
  };
}

export function getEventsFail(error) {
  return {
    type: GET_EVENTS_FAIL,
    error,
  };
}

export function deleteEvent(eventId) {
  return {
    type: DELETE_EVENT,
    eventId,
  };
}

export function deleteEventSuccess(event) {
  return {
    type: DELETE_EVENT_SUCCESS,
    event,
  };
}

export function deleteEventFail(error) {
  return {
    type: DELETE_EVENT_FAIL,
    error,
  };
}
