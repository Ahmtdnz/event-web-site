import {
  GET_SUBCATEGORIES,
  GET_SUBCATEGORIES_SUCCESS,
  GET_SUBCATEGORIES_FAIL,
  DELETE_SUBCATEGORY,
  DELETE_SUBCATEGORY_SUCCESS,
  DELETE_SUBCATEGORY_FAIL,
} from './constants';


const initial = {
  subcategories: {
    subcategories: [],
    deleteSubcategoryProcess: '',
  },
};

export default function app(state = initial.subcategories, action) {
  switch (action.type) {
    case GET_SUBCATEGORIES:
      return { ...state };
    case GET_SUBCATEGORIES_SUCCESS:
      return { ...state, subcategories: action.categories.data };
    case GET_SUBCATEGORIES_FAIL:
      return { ...state };
    case DELETE_SUBCATEGORY:
      return { ...state };
    case DELETE_SUBCATEGORY_SUCCESS:
      return { ...state, deleteSubcategoryProcess: true };
    case DELETE_SUBCATEGORY_FAIL:
      return { ...state, deleteSubcategoryProcess: false };
    default:
      return state;
  }
}
