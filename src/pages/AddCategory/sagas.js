import { call, put } from 'redux-saga/effects';
import {
  addCategorySuccess,
  addCategoryFail,
} from './actions';
import request from '../../utils/request';

const API = require('../../utils/api');

export default function* addCategory({ data }) {
  const addCategoryRequestURL = `${API.api_url}categories?access_token=${localStorage.getItem('token')}`;
  try {
    const addCategoryResult = yield call(request, addCategoryRequestURL, 'POST', data);
    yield put(addCategorySuccess(addCategoryResult));
  } catch (error) {
    yield put(addCategoryFail());
  }
}
