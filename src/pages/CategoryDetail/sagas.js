import { call, put } from 'redux-saga/effects';
import {
  getCategorySuccess,
  getCategoryFail,
  updateCategorySuccess,
  updateCategoryFail,
} from './actions';
import request from '../../utils/request';

const API = require('../../utils/api');

export default function* getCategory({ categoryId, type, data }) {
  if (type === 'GET_CATEGORY') {
    const categoryRequestURL = `${API.api_url}categories/${categoryId}?access_token=${localStorage.getItem('token')}`;
    try {
      const categoryResult = yield call(request, categoryRequestURL, 'GET');
      yield put(getCategorySuccess(categoryResult));
    } catch (error) {
      yield put(getCategoryFail());
    }
  } else if (type === 'UPDATE_CATEGORY') {
    const updateCategoryRequestURL = `${API.api_url}categories?access_token=${localStorage.getItem('token')}`;
    try {
      const updateCategoryResult = yield call(request, updateCategoryRequestURL, 'PUT', data);
      yield put(updateCategorySuccess(updateCategoryResult));
    } catch (error) {
      yield put(updateCategoryFail());
    }
  }
}
