import {
  GET_CATEGORY,
  GET_CATEGORY_SUCCESS,
  GET_CATEGORY_FAIL,
  UPDATE_CATEGORY,
  UPDATE_CATEGORY_SUCCESS,
  UPDATE_CATEGORY_FAIL,
} from './constants';

export function getCategory(categoryId) {
  return {
    type: GET_CATEGORY,
    categoryId,
  };
}

export function getCategorySuccess(event) {
  return {
    type: GET_CATEGORY_SUCCESS,
    event,
  };
}

export function getCategoryFail(error) {
  return {
    type: GET_CATEGORY_FAIL,
    error,
  };
}

export function updateCategory(data) {
  return {
    type: UPDATE_CATEGORY,
    data,
  };
}

export function updateCategorySuccess(event) {
  return {
    type: UPDATE_CATEGORY_SUCCESS,
    event,
  };
}

export function updateCategoryFail(error) {
  return {
    type: UPDATE_CATEGORY_FAIL,
    error,
  };
}
