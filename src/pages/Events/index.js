import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';
import * as classNames from 'classnames';
import { BootstrapTable, TableHeaderColumn, DeleteButton, InsertButton } from 'react-bootstrap-table';
import { getEvents, deleteEvent } from './actions';

class Events extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedItem : '',
    };
    this.handleRowSelect = this.handleRowSelect.bind(this);
    this.createCustomDeleteButton = this.createCustomDeleteButton.bind(this);
    this.createCustomInsertButton = this.createCustomInsertButton.bind(this);
  }
  componentWillMount() {
    this.props.dispatch(getEvents());
  }

  handleRowSelect(row) {
    this.setState({ selectedItem: row });
  }

  delete() {
    this.props.dispatch(deleteEvent(this.state.selectedItem.id));
  }

  createCustomDeleteButton(onClick) {
    const wrapperClass = classNames({
      'btn': true,
      'btn-warning': true,
      'disabled': this.state.selectedItem === '',
    });
    return (
      <DeleteButton
        btnText='Delete'
        btnContextual='btn-warning'
        className={wrapperClass}
        btnGlyphicon='glyphicon-remove'
        onClick={() => this.delete()}
      />
    );
  }

  createCustomInsertButton = (onClick) => {
    const wrapperClass = classNames({
      'btn': true,
      'btn-info': true,
      'disabled': this.state.selectedItem === '',
    });
    return (
      <InsertButton
        btnText='Event Detail'
        btnContextual='btn-info'
        className={wrapperClass}
        btnGlyphicon='glyphicon-edit'
        onClick={ () => this.props.history.push(`/event/${this.state.selectedItem.id}`) }/>
    );
  }

  render() {
    let { events } = this.props;
    const selectRow = {
      mode: 'radio',
      onSelect: this.handleRowSelect,
    };
    const options = {
      deleteBtn: this.createCustomDeleteButton,
      insertBtn: this.createCustomInsertButton,
    };
    return (
      <div className="container" style={{marginTop: 75}}>
        <Link
          to="/event/add"
          className="btn btn-success"
          style={{marginBottom: 20}}
        >
          Add Event
        </Link>
        <BootstrapTable
          data={events}
          striped
          hover
          search
          selectRow={selectRow}
          options={options}
          deleteRow
          insertRow
        >
          <TableHeaderColumn isKey dataField="id">Event Id</TableHeaderColumn>
          <TableHeaderColumn dataField="title">Title</TableHeaderColumn>
          <TableHeaderColumn dataField="description">Description</TableHeaderColumn>
          <TableHeaderColumn dataField="subcategoryId">SubCategory Id</TableHeaderColumn>
        </BootstrapTable>
      </div>
    );
  }
}

const mapToProps = ({ app, events }) => (
  {
    app,
    events: events.events,
  }
);

export default withRouter(connect(mapToProps)(Events));
