import { call, put } from 'redux-saga/effects';
import {
  getEventSuccess,
  getEventFail,
  updateEventSuccess,
  updateEventFail,
} from './actions';
import request from '../../utils/request';

const API = require('../../utils/api');

export default function* getEvent({ eventId, type, data }) {
  if (type === 'GET_EVENT') {
    const eventRequestURL = `${API.api_url}events/${eventId}?access_token=${localStorage.getItem('token')}`;
    try {
      const eventResult = yield call(request, eventRequestURL, 'GET');
      yield put(getEventSuccess(eventResult));
    } catch (error) {
      yield put(getEventFail());
    }
  } else if (type === 'UPDATE_EVENT') {
    const updateEventRequestURL = `${API.api_url}events?access_token=${localStorage.getItem('token')}`;
    try {
      const updateEventResult = yield call(request, updateEventRequestURL, 'PUT', data);
      yield put(updateEventSuccess(updateEventResult));
    } catch (error) {
      yield put(updateEventFail());
    }
  }
}
