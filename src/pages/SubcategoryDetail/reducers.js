import {
  GET_SUBCATEGORY,
  GET_SUBCATEGORY_SUCCESS,
  GET_SUBCATEGORY_FAIL,
  UPDATE_SUBCATEGORY,
  UPDATE_SUBCATEGORY_SUCCESS,
  UPDATE_SUBCATEGORY_FAIL,
} from './constants';


const initial = {
  subcategory: {
    subcategory: {},
    updateSubcategoryProcess: '',
  },
};

export default function app(state = initial.subcategory, action) {
  switch (action.type) {
    case GET_SUBCATEGORY:
      return { ...state };
    case GET_SUBCATEGORY_SUCCESS:
      return { ...state, subcategory: action.event.data };
    case GET_SUBCATEGORY_FAIL:
      return { ...state, error: 'error' };
    case UPDATE_SUBCATEGORY:
      return { ...state };
    case UPDATE_SUBCATEGORY_SUCCESS:
      return { ...state, updateSubcategoryProcess: true };
    case UPDATE_SUBCATEGORY_FAIL:
      return { ...state, updateSubcategoryProcess: false };
    default:
      return state;
  }
}
