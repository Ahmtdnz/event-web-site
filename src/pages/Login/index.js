import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import { withRouter } from 'react-router-dom';
import { login } from './actions';

class AddEvent extends Component {
  submit(e) {
    e.preventDefault();
    this.props.dispatch(login(this.props.form.values));
  }
  componentWillReceiveProps = (nextProps) => {
    if(nextProps.login.loginProcess) {
      this.props.history.push('/categories');
    }
  }
  
  render() {
    return (
      <div className="container" style={{marginTop: 75}}>
        <form className="col-md-6 col-md-offset-3">
          <div className="form-group">
            <label>Username</label>
            <Field
              name="username"
              component="input"
              className="form-control"
              type="text"
              placeholder="Ahmet"
            />
          </div>
          <div className="form-group">
            <label>Password</label>
            <Field
              name="password"
              component="input"
              className="form-control"
              type="password"
              placeholder="123456"
            />
          </div>
          <button onClick={(e) => this.submit(e)} className="btn btn-primary">Login</button>
        </form>
      </div>
    );
  }
}

const mapToProps = ({ form, login }) => (
  {
    form: form['login'],
    login,
  }
);

const wr = withRouter(connect(mapToProps)(AddEvent));

export default reduxForm({
  form: 'login',
  initialValues: {
    username: '',
    password: '',
  },
})(wr);
