import { call, put } from 'redux-saga/effects';
import {
  addSubcategorySuccess,
  addSubcategoryFail,
} from './actions';
import request from '../../utils/request';

const API = require('../../utils/api');

export default function* addSubcategory({ data }) {
  const addSubcategoryRequestURL = `${API.api_url}subcategories?access_token=${localStorage.getItem('token')}`;
  try {
    const addSubcategoryResult = yield call(request, addSubcategoryRequestURL, 'POST', data);
    yield put(addSubcategorySuccess(addSubcategoryResult));
  } catch (error) {
    yield put(addSubcategoryFail());
  }
}
