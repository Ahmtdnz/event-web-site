import React, { Component } from 'react';
import { Switch, Route, Link, NavLink, Redirect } from 'react-router-dom';
import Events from '../../pages/Events';
import Categories from '../../pages/Categories';
import Subcategories from '../../pages/SubCategories';
import AddEvent from '../../pages/AddEvent';
import EventDetail from '../../pages/EventDetail';
import AddCategory from '../../pages/AddCategory';
import CategoryDetail from '../../pages/CategoryDetail';
import AddSubcategory from '../../pages/AddSubcategory';
import SubcategoryDetail from '../../pages/SubcategoryDetail';
import Login from '../../pages/Login';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    if (!localStorage.token) {
      return (
        <Login />
      );
    }
    return (
      <div>
        <nav className="navbar navbar-inverse navbar-fixed-top">
          <div className="container-fluid">
            <div className="navbar-header">
              <button type="button" className="navbar-toggle collapsed">
                <span className="sr-only">Toggle navigation</span>
                <span className="icon-bar" />
                <span className="icon-bar" />
                <span className="icon-bar" />
              </button>
              <Link
                className="navbar-brand"
                to="/"
              >
                Project name
              </Link>
            </div>
          </div>
        </nav>
        <div className="container-fluid">
          <div className="row">
            <div className="col-md-2 sidebar">
              <ul className="nav nav-sidebar">
                <li>
                  <NavLink to="/events" activeClassName="active">Events</NavLink>
                </li>
                <li>
                  <NavLink to="/categories" activeClassName="active">Categories</NavLink>
                </li>
                <li>
                  <NavLink to="/subcategories" activeClassName="active">SubCategories</NavLink>
                </li>
              </ul>
            </div>
            <div className="col-md-10 main col-md-offset-2">
              <Switch>
                <Route path="/events" component={Events} />
                <Route path="/categories" component={Categories} />
                <Route path="/category/add" component={AddCategory} />
                <Route path="/category/:id" component={CategoryDetail} />
                <Route path="/subcategories" component={Subcategories} />
                <Route path="/subcategory/add" component={AddSubcategory} />
                <Route path="/subcategory/:id" component={SubcategoryDetail} />
                <Route path="/event/add" component={AddEvent} />
                <Route path="/event/:id" component={EventDetail} />
                <Redirect from="/" to="/events" />
              </Switch>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
