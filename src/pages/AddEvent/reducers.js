import {
  ADD_EVENT,
  ADD_EVENT_SUCCESS,
  ADD_EVENT_FAIL,
} from './constants';


const initial = {
  addEvent: {
    addEventProcess: '',
  },
};

export default function app(state = initial.addEvent, action) {
  switch (action.type) {
    case ADD_EVENT:
      return { ...state };
    case ADD_EVENT_SUCCESS:
      return { ...state, addEventProcess: true };
    case ADD_EVENT_FAIL:
      return { ...state, addEventProcess: false };
    default:
      return state;
  }
}
